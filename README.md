# JerrumsFilter

A simple implementation of Jerrum's filter in C++.

Jerrum's Filter is an algorithm that computes the minimum number of generators
of a permutation group given by a (not necessarily minimal) generating set.

An elementary interface is also included.

This was originally a project I completed for a course in computer science
during my undergraduate program at the Budapest University of Technology.
