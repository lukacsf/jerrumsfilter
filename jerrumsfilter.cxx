#include<iostream>
#include<vector>
#include<map>
#include<algorithm>
#include<string>

template<typename T>
std::vector<T> remove( T e, std::vector<T> v ){
	for( int i = 0; i < static_cast<int>(v.size()); i++ ){
		if( e == v[i] ){
			v.erase( v.begin()+i );
			return v;
		}
	}
	return v;
}

class perm {
protected:
	int n;
	std::vector<int> pm;
public:
	perm( void );
	perm( std::vector<int> );
	perm( int );
	perm( perm const & );
	int len( void ) const { return n; }
	perm inv( void );
	int operator()( int i ) const { return pm[i]; }
	perm operator*( perm const & );
	bool operator==( perm & );
	bool operator==( perm const & );
	bool ide( void );
};

class permGroup {
	int n;
	std::vector<perm> genSys;
	void updateGenSys( std::vector<perm> v ){ genSys = v; }
	void clean( void );
public:
	permGroup( void );
	permGroup( std::vector<perm> );
	std::vector<perm> getGenSys( void ){ return genSys; }
	int getN( void ){ return n; }
	void Jerrum( void );
};

class jerrumGraph {
	int n;										// gráf csúcsainak száma
	std::map<int, std::vector<int> > map;		// él: {szomszédok vektora} formában tárolja a gráfot
	std::vector<std::vector<perm>> matrix;		// adjacenciamátrix, perm típusú elemekkel
public:
	jerrumGraph( permGroup );
	std::vector<int> operator()( int i ){ return map[i]; }
	std::vector<perm> operator[]( int i ){ return matrix[i]; }
	std::map<int, std::vector<int>> getMap( void ){ return map; }
	std::vector<std::vector<perm>> getMat( void ){ return matrix; }
};

std::vector<int> korkeres( std::map<int, std::vector<int>>, int );

int kovetkezo( int, std::vector<int> );

perm::perm( void ){
	n = 1;
	pm.push_back(0);
}

perm::perm( std::vector<int> v ){
	n = static_cast<int>(v.size());
	for( int x : v ){
		pm.push_back(x);
	}
}

perm::perm( int k ){
	n = k;
	for( int i = 0; i < k; i++ ){
		pm.push_back(i);
	}
}

perm::perm( perm const & other ){
	n = other.len();
	for( int i = 0; i < n; i++ ){
		pm.push_back( other(i) );
	}
}

perm perm::inv( void ){
	std::vector<int> v;
	for( int i = 0; i < n; i++ ){
		v.push_back(0);
	}
	for( int i = 0; i < n; i++ ){
		v[ pm[i] ] = i;
	}
	perm temp(v);
	return temp;
}

perm perm::operator*( perm const & other){
	if( n != other.len() ){
		throw std::logic_error("The lengths have to match");
	}
	std::vector<int> v{};
	for( int i = 0; i < n; i++ ){
		v.push_back( other( pm[i] ) );
	}
	perm temp(v);
	return temp;
}

bool perm::operator==( perm & other ){
	if( n != other.len() ){
		return 0;
	}
	for( int i = 0; i < n; i++ ){
		if( pm[i] != other(i) ){
			return 0;
		}
	}
	return 1;
}

bool perm::operator==( perm const & other ){
	if( n != other.len() ){
		return 0;
	}
	for( int i = 0; i < n; i++ ){
		if( pm[i] != other(i) ){
			return 0;
		}
	}
	return 1;
}

bool perm::ide( void ){
	for( int i = 0; i < n ; i++ ){
		if( pm[i] != i ){
			return 0;
		}
	}
	return 1;
}

permGroup::permGroup( void ){
	n = 1;
	std::vector<int> v{ 1 };
	perm i(v);
	genSys.push_back(i);
}

permGroup::permGroup( std::vector<perm> gen ){
	n = gen[0].len();
	for( perm p : gen ){
		if( n != p.len() ){
			throw std::logic_error("The lengths have to match");
		}
	}
	for( perm p : gen ){
		genSys.push_back( p );
	}
}

void permGroup::clean( void ){
	for( perm p : genSys ){
		if( p.ide() ){
			genSys = remove( p, genSys );
		}
	}
}

void permGroup::Jerrum(){
	this->clean();
	while( 1 ){
		// Letrehozzuk a grafot:

		jerrumGraph G{ *this };
		std::map<int, std::vector<int> > map = G.getMap();

		// Megkeressuk, hogy melyik a legkisebb pont, ahonnan van kor
		// Ha a graf kormentes, akkor akkor leallunk
		std::vector<int> kor;
		for( int i = 0; i < this->getN(); i++ ){
			kor = korkeres( map, i );
			if( kor.size() > 2 ){
				break;
			}
		}
		if( kor.size() == 2 ){
			break;
		}
		
		// Innentol »kor« jol mukodik
		// Most meg kell konstrualni az uj generatorlemet: ez lesz pmt
		perm pmt( this->getN() );

		// A segmentation fault szigorúan ez után van
		for( int i = 1; i < static_cast<int>(kor.size())-1; i++ ){
			pmt = pmt * G[ kor[i+1] ][ kor[i] ];
		}
		
		// Megkeressük, hogy a generátorrendszer melyik elemét cseréljük le pmt-re

		perm csere( this->getN() );
		for( perm q : this->getGenSys() ){
			if( q( kor[1] ) == kor[2] ){
				csere = q;
				break;
			}
		}

		std::vector<perm> newGenSys = remove( csere, this->getGenSys() );
		newGenSys.push_back( pmt );

		this->updateGenSys( newGenSys );
		this->clean();
	}
}

jerrumGraph::jerrumGraph( permGroup G ){

	std::vector<perm> genek = G.getGenSys();
	
	for( perm p : genek ){
		for( int k = 0; k < G.getN(); k++ ){
			if( p(k) != k ){
				map[k].push_back( p(k) );
				map[ p(k) ].push_back(k);
				break;
			}
		}
	}
	for( int i = 0; i < G.getN(); i++ ){
		std::vector<perm> sor;
		for( int j = 0; j < G.getN(); j++){
			perm ep( G.getN() );
			sor.push_back(ep);
		}
		matrix.push_back(sor);
	}
	for( perm p : genek){
		for( int i = 0; i < p.len(); i++ ){
			
			if( p(i) != i ){
				perm q( { 0, 2, 1, 3, 4 } );
				matrix[i][ p(i) ] = q;
				matrix[ p(i) ][i] = q.inv();
				break;
			}
		}
	}
}

std::vector<int> korkeres( std::map<int, std::vector<int>> graf, int start ){
	//elkeszitjuk a grafnak azt a verziojat, ami koveti, hogy melyik csucsbol melyik csucsot probaltuk mar
	//innen minden probalkozaskor kivesszuk a az adott csucsot, hogy latszodjon, hogy mar probaltuk
	std::map<int, std::vector<int>> graf_volt = graf;

	std::vector<int> cycle{ -1, start };
	int akt = start;
	int prev = -1;
	int i = 0;
	while( i < 15 ){
		// Egy ciklust karakterizál: graf_volt cycle
		akt = cycle[ cycle.size()-1 ];
		prev = cycle[ cycle.size()-2 ];

		// Megkeressuk az alkalmas kovetkezot:
		// Kovetelmenyek: - next != prev
		//				  - next is in graf[ akt ]
		//				  - next maximalis
		int next = kovetkezo( prev, graf_volt[ akt ] );
		if( next == -3 ){
			return cycle;
		}
		graf_volt[ akt ] = remove( next, graf_volt[ akt ] );
		if( next == -2 ){
			cycle.pop_back();
			continue;
		}

		cycle.push_back( next );		// hozzavesszuk a megtalalt kovi csucsot
		// Ekkor megvan minden ahhoz, hogy a következő ciklusba léphessünk

		if( next == start ){
			return cycle;			// ha ezzel pont korbeertunk, akkor az algoritmus leall
		}
		++i;
	}
	return cycle;
}

int kovetkezo( int p, std::vector<int> v ){

	if( p == -1 && v.size() == 0 ){
		return -3;
	}

	int max = -2;
	for( int x : v){
		if( x > max && x != p ){
			max = x;
		}
	}
	return max;
}

int answer( std::string question ){
     std::cout << question;
     std::string answ;
     getline( std::cin, answ );
     return std::stoi(answ);
 }

std::string answers( std::string question ){
     std::cout << question;
     std::string answ;
     getline( std::cin, answ );
     return answ;
 }


void tutorial( void ){
	perm a( { 0, 4, 2, 3, 1 } );
	perm b( { 4, 1, 0, 2, 3 } );
	perm c( { 2, 1, 0, 3, 4 } );
	perm d( { 0, 2, 4, 3, 1 } );
	perm e( { 0, 1, 3, 4, 2 } );
	permGroup G( { a, b, c, d, e } );
	jerrumGraph J( G );
	G.Jerrum();
	std::cout << "In this tutorial we are going to see how the algorithm works on a subgroup of S_5" << std::endl
			  << "The convention for representing permutations is the following: the permutation a brings i to the ith element of its constructor list" << std::endl 
			  << "The generators of the group in question, G<S_5 are the following:" << std::endl
			  << "a = ( 0, 4, 2, 3, 1 )" << std::endl
			  << "b = ( 4, 1, 0, 2, 3 )" << std::endl
			  << "c = ( 2, 1, 0, 3, 4 )" << std::endl
			  << "d = ( 0, 2, 4, 3, 1 )" << std::endl
			  << "e = ( 0, 1, 3, 4, 2 )" << std::endl
			  << "Thus G = < a, b, c, d, e >." << std::endl
			  << "After running the Jerrum filter algorithm on the group, we get that the minimal generator system for G consists of "
			  << G.getGenSys().size() << " elements" << std::endl;
}

void interface( void ){
	int k = answer( "Which symmetric group is the desired group a subgroup of? (Please enter apositive integer) " );
	std::cout << "Now pleae enter the generators of you group." << std::endl;
	std::vector<perm> gens;
	std::string s = "y";
	while( s == "y" ) {
		std::vector<int> v;
		for( int i = 0; i < k; i++ ){
			std::cout << i << "--> ";
			int a = answer( "" );
			v.push_back( a );
		}
		perm p( v );
		gens.push_back( p );
		std::cout << "Generator added" << std::endl;
		s = answers( "Do you want to add another generator? (y or n) " );
	}
	permGroup G( gens );
	std::cout << "Running Jerrum filter algorithm..." << std::endl;
	G.Jerrum();
	std::cout << "The size of the minimal generator system is " << G.getGenSys().size() << " elements" << std::endl;
}

int main( void ){
	std::string s = answers( "Do you want to see a tutorial, or make your own example? (Type t for tutorial or o for own example) ");
	if( s == "o" ){
		interface();
	}
	if( s == "t" ){
		tutorial();
	}
	return 0;
}







